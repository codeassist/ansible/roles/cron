"""
Role tests
"""

import os
import pytest

from testinfra.utils.ansible_runner import AnsibleRunner

testinfra_hosts = AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


@pytest.mark.parametrize('srv_name,distribution', [
    ('cron', 'debian'),
    ('cron', 'ubuntu'),
    ('crond', 'centos')
])
def test_service(host, srv_name, distribution):
    """
    Check if service is enabled and running
    """

    if distribution == '*' or distribution == host.system_info.distribution.lower():
        cron = host.service(srv_name)

        assert cron.is_enabled
        assert cron.is_running

    else:
        pytest.skip('Test is not defined for this distribution')
