|Branch|Status|
|------|:--------:|
|master|[![pipeline status](https://gitlab.itsupportme.by/ansible-cm/roles/cron/badges/master/pipeline.svg)](https://gitlab.itsupportme.by/ansible-cm/roles/cron/commits/master)
|develop|[![pipeline status](https://gitlab.itsupportme.by/ansible-cm/roles/cron/badges/develop/pipeline.svg)](https://gitlab.itsupportme.by/ansible-cm/roles/cron/commits/develop)

## Ansible Role
### **_cron_**

Setup scheduled tasks via various cron-daemon implementations on Debian/Ubuntu, RedHat/CentOS and Alpine Linux systems.

## Requirements

  - Ansible 2.5 and higher

## Role Dependencies (roles, packages etc.)

none

## License

MIT

## Author Information

This role was created in 2018 by ITSupportMe, LLC.
